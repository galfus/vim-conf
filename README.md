### Instalación

Haz primero una copia del fichero .vimrc y de la carpeta .vim que haya en tu equipo (si existen).
Luego:

```sh
$ cd ~
$ git clone --depth 1 https://galfus@bitbucket.org/galfus/vim-conf.git
$ mv vim-conf .vim
$ cp .vim/.vimrc .
```