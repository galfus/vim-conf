
" Plugins
" -------

call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
Plug 'junegunn/seoul256.vim'
Plug 'junegunn/vim-easy-align'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" Using git URL
Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" Plugin options
Plug 'nsf/gocode', { 'tag': 'go.weekly.2012-03-13', 'rtp': 'vim' }

" Plugin outside ~/.vim/plugged with post-update hook
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }

" Unmanaged plugin (manually installed and updated)
Plug '~/my-prototype-plugin'

call plug#end()

Plug 'whatyouhide/vim-gotham'

execute pathogen#infect()

let g:NERDTreeIgnore=['\~$', 'node-modules', 'vendor']


" Ajustes
" -------

set nocompatible      " We're running Vim, not Vi!
set number
set expandtab
set tabstop=2 shiftwidth=2 softtabstop=2
set autoindent
syntax on
filetype plugin indent on
autocmd Filetype java setlocal ts=4 sts=4 sw=4
autocmd Filetype kotlin setlocal ts=4 sts=4 sw=4
autocmd Filetype javascript setlocal ts=2 sts=2 sw=2
set antialias
set t_Co=256
set autoindent
set backspace=indent,eol,start
set encoding=utf-8
set fileencoding=utf-8
set gdefault
set guioptions-=Be
set guioptions=aAc
set hlsearch
set ignorecase
set incsearch
set listchars=tab:▸\ ,eol:¬,nbsp:⋅,trail:•
set noswapfile
set number
set shell=/bin/bash
set showmatch
set smartcase
set term=screen-256color
set ts=2 sts=2 sw=2 expandtab
set background=dark
set cursorline
set wildmenu
set foldenable
set pastetoggle=<F2>
set nowrap
colorscheme gotham256


" Mapeos
" ------

let mapleader = ","
imap kk <esc>
imap jj <esc>
imap <leader>e <esc><esc>
nnoremap <C-s> :w<CR>
nnoremap <leader>m :NERDTreeFocus<CR>
nnoremap <leader>n :NERDTreeToggle<CR>
nnoremap <leader>b :b<space>
map K 5k
map J 5j
map <leader>e <esc><esc>
map <leader>a ^
map <leader>s $
map <leader>C ^C
map <leader>D ^D
map <leader>v :vs<CR>
map <leader>h :sp<CR>
nnoremap <leader>d :bd<CR> 
nnoremap <leader>f :b#<CR> 
nnoremap <leader>w :w<CR>
nnoremap <leader>r :!!<CR>
nnoremap <leader>q :q<CR>
nnoremap <leader>wr :w<CR>:!!<CR>
nnoremap <leader>p :set paste<CR>
nnoremap <leader><cr> :noh<CR> :set nopaste<CR>
nnoremap <leader>l :ls<CR>:b<space>
nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-H> <C-W>h
nnoremap <C-L> <C-W>l
inoremap <leader>; <C-o>A;

autocmd BufNewFile,BufReadPost *.md set filetype=markdown
autocmd BufNewFile,BufRead *.vue set filetype=javascript

" Delete trailing whitespace on save
autocmd FileType c,cpp,java,php,html,css,js,kt,rb,scss,vue autocmd BufWritePre <buffer> %s/\s\+$//e

" Ajustes no usados ya
" --------------------

"set smartindent
"set visualbell
"set winheight=999
"set winheight=5
"set winminheight=5
"set winwidth=84
"set guifont=Inconsolata\ 10.5
"set guifont=SourceCodePro
"set colorcolumn=80
"set list
"autocmd Filetype groovy setlocal ts=4 sts=4 sw=4


" Mapeos no usados ya
" -------------------

"inoremap <C-l> <space>=><space>
"nmap <c-k> :w<CR>
"imap jj <c-o>
"imap <leader>d <esc>
"imap <S-CR> <CR><CR>end<Esc>-cc
"nnoremap <leader>ev :vs $MYVIMRC<CR>
"nnoremap <leader>gs :Gstatus<CR><C-W>15+
"nnoremap <leader>rs :!clear;bundle exec rake<CR>
"nnoremap <leader>a :Ack 
"nnoremap <leader>a ^ 
"nnoremap <leader>s $ 
"nnoremap <c-i> ^
"nnoremap <c-a> $
"nnoremap <leader>f $ 
"nnoremap <leader>t :CtrlP<CR>
"nnoremap n nzz
"nnoremap N Nzz
"nnoremap <leader>r :!clear; rspec %<CR>
"inoremap <C-Space> <C-x><C-o>
"inoremap <C-@> <C-Space>
"nnoremap <leader>ra :!clear; rspec<CR>
"nnoremap <leader>wr :w<CR>:!!<CR>
" nnoremap § 5k 
" nnoremap ¶ 5j 
" vnoremap § 5k 
" vnoremap ¶ 5j 

